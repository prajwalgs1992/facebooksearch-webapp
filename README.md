# README #

Web API to fetch the Facebook user,group,page,location & events data using Facebook Graph API.

### Website link ###
Hosted on Amazon AWS cloud server.
http://sample-env.kj5iax3tk2.us-west-2.elasticbeanstalk.com/index.php/

### Summary of support tools used ###

* Facebook graph API(v2.8)
* Facebook PHP 5.5 SDK
* Facebook graph explorer
